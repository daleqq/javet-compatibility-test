import com.caoccao.javet.interop.V8Host;
import com.caoccao.javet.interop.V8Runtime;
import com.caoccao.javet.exceptions.JavetException;

class JavetMain {
  public static void main(String[] args) {
    try (V8Runtime v8Runtime = V8Host.getV8Instance().createV8Runtime()) {
      System.out.println(v8Runtime.getExecutor("'Hello Javet'").executeString());
    } catch (JavetException e) {
      throw new RuntimeException(e);
    }
  }
}

